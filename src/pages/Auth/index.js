import React from 'react';
import { useHistory } from 'react-router-dom';
import PageLayout from '../../layouts/PageLayout';
import SectionLayout from '../../layouts/SectionLayout';
import Login from '../../containers/forms/Login';
import Signup from '../../containers/forms/Signup';
import Confirm from '../../containers/forms/Confirm';

//Component
const _AuthPage = ({ type = 'login' }) => {
  const { history } = useHistory();
  const [_type, setType] = React.useState(type);

  //
  const handleSignup = (username, password) => {
    setType('signup');
  };

  //
  const handleConfirm = (username, password, code) => {
    setType('login');
  };

  const handleBack = () => {
    if (_type !== 'login') {
      setType('login');
      return;
    }

    if (!!history) history.push('/');
    return;
  };

  //Render
  return (
    <PageLayout>
      <SectionLayout id="SectionCategories__" justify="center" align="center" height="fixed">
        {!!_type && _type === 'login' && (
          <Login
            onSubmit={() => handleBack()}
            onForgot={() => console.log('Forgot')}
            onSignup={handleSignup}
            onClose={() => handleBack()}
          />
        )}
        {!!_type && _type === 'signup' && (
          <Signup
            onSubmit={() => handleBack()}
            onForgot={() => console.log('Forgot')}
            onSignup={() => console.log('Signup')}
            onClose={() => handleBack()}
          />
        )}
        {!!_type && _type === 'confirm' && <Confirm onSubmit={handleConfirm} onClose={() => handleBack()} />}
      </SectionLayout>
    </PageLayout>
  );
};

const AuthPage = React.memo(_AuthPage);
export default AuthPage;
