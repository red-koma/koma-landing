import React from 'react';
import { Route, Redirect, useLocation } from 'react-router-dom';

export default function AuthenticatedRoute({ children, ...rest }) {
  const { pathname, search } = useLocation();
  const [isAuthenticated] = React.useState(false);

  return (
    <Route {...rest}>{isAuthenticated ? children : <Redirect to={`/login?redirect=${pathname}${search}`} />}</Route>
  );
}
