import styled from 'styled-components';

export const Header = styled.div`
  height: 7vh;
  width: 100vw;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Logo = styled.div`
  height: 35px;
  width: 70px;
  background-color: yellow;
  margin: 0 20px;
`;

export const ButtonSet = styled.div`
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const BodyContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
`;

export const SearchContainer = styled.div`
  height: 100%;
  width: 20vw;
  background-color: gray;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

export const RightContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

export const ResultsContainer = styled.div`
  width: 80vw;
  background-color: silver;
  display: grid;
  grid-gap: 0px;
  grid-template-columns: 30% 30% 30%;
  grid-auto-rows: 30vh;
  align-items: center;
  justify-content: center;
  padding: 20px 0 20px;
`;

export const Dropdown = styled.div`
  height: 35px;
  width: 80%;
  margin: 5px 10px;
  background-color: cyan;
`;

export const InputSlider = styled.div`
  height: 25px;
  width: 80%;
  margin: 10px 10px;
  background-color: blue;
`;

export const Property = styled.div`
  height: 95%;
  width: 95%;
  background-color: gold;
  align-self: center;
  justify-self: center;
`;
