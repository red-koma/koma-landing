import React from 'react';

import PageLayout from '../../layouts/PageLayout';
import SectionLayout from '../../layouts/SectionLayout';
import Button from '../../components/buttons/Button';

import {
  Header,
  ButtonSet,
  Logo,
  BodyContainer,
  SearchContainer,
  ResultsContainer,
  Dropdown,
  InputSlider,
  RightContainer,
  Property,
} from './styles';
//Component
export const _SearchPage = props => {
  //Render
  return (
    <PageLayout>
      <SectionLayout id="SectionSearch__Header" justify="center" align="center" height="auto">
        <Header id="Header">
          <Logo id="Logo" />
          <ButtonSet>
            <Button size="small" id="Back">
              Back
            </Button>
            <Button size="small" id="Login">
              Login
            </Button>
          </ButtonSet>
        </Header>
      </SectionLayout>
      <SectionLayout id="SectionSearch__Body" justify="center" align="center" height="auto">
        <BodyContainer id="BodyContainer">
          <SearchContainer id="SearchContainer">
            <Dropdown id="Dropdown" />
            <Dropdown id="Dropdown" />
            <Dropdown id="Dropdown" />
            <Dropdown id="Dropdown" />
            <InputSlider id="InputSlider" />
            <InputSlider id="InputSlider" />
            <InputSlider id="InputSlider" />
            <InputSlider id="InputSlider" />
          </SearchContainer>
          <RightContainer>
            <ResultsContainer id="ResultsContainer">
              <Property id="Property" />
              <Property id="Property" />
              <Property id="Property" />
              <Property id="Property" />
              <Property id="Property" />
              <Property id="Property" />
              <Property id="Property" />
              <Property id="Property" />
              <Property id="Property" />
              <Property id="Property" />
            </ResultsContainer>
            <Button size="large" id="Login">
              More results
            </Button>
          </RightContainer>
        </BodyContainer>
      </SectionLayout>
    </PageLayout>
  );
};

const SearchPage = React.memo(_SearchPage);
export default SearchPage;
