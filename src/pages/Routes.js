import React from 'react';
import { Route, Switch } from 'react-router-dom';

// Routes
import Home from './Home';
import Search from './Search';
import { NotFound } from './NotFound';
// import AuthenticatedRoute from './AuthenticatedRoute';
import UnauthenticatedRoute from './UnauthenticatedRoute';
//
import Login from './Auth';

export default () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <UnauthenticatedRoute path="/auth">
        <Login />
      </UnauthenticatedRoute>
      <Route path="/search">
        <Search />
      </Route>
      {/* Finally, catch all unmatched routes */}
      <Route>
        <NotFound />
      </Route>
    </Switch>
  );
};
