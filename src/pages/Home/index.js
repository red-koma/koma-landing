import React from 'react';
import { useHistory } from 'react-router-dom';
import PageLayout from '../../layouts/PageLayout';
import SectionLanding from '../../containers/sections/SectionLanding';
import SectionFeatured from '../../containers/sections/SectionFeatured';
import SectionCategories from '../../containers/sections/SectionCategories';
import SectionCities from '../../containers/sections/SectionCities';
import SectionActions from '../../containers/sections/SectionActions';
// import SignUp from './../../containers/SignUp';

import SectionCallExpert from '../../containers/sections/SectionCallExpert';
//Component
const HomePage = props => {
  const history = useHistory();

  //
  const goLogin = () => {
    history.push('/auth');

    return;
  };

  //Render
  return (
    <PageLayout>
      <SectionLanding onLogin={goLogin()} />
      <SectionFeatured />
      <SectionCategories />
      <SectionCities />
      <SectionActions />
      <SectionCallExpert />
    </PageLayout>
  );
};

const Home = React.memo(HomePage);
export default Home;
