import styled from 'styled-components';

//Apply Styled Component.
export const StyledComponent = styled.div`
  display: flex;
  width: 100%;
  height: ${props => {
    return props.height === 'fixed' ? '80vh' : props.height === 'auto' ? 'auto' : props.height;
  }};

  flex-direction: column;
  justify-content: ${props => {
    switch (props.justify) {
      case 'top':
        return 'flex-start';
      case 'center':
        return 'center';
      case 'bottom':
        return 'flex-end';

      default:
        return 'flex-start';
    }
  }};

  align-items: ${props => {
    switch (props.align) {
      case 'left':
        return 'flex-start';
      case 'center':
        return 'center';
      case 'right':
        return 'flex-end';

      default:
        return 'center';
    }
  }};
  z-index: ${props => props.order};
`;

export const Container = styled.section`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 20%;
  margin: 5vh 0px 10px;
`;

export const Title = styled.span`
  max-width: 80vw;
  font-weight: 700;
  text-align: center;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.title};
  color: ${props => props.theme.colors.title};
`;

export const Description = styled.span`
  width: 80vw;
  font-weight: 400;
  text-align: center;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.subtitle};
  color: ${props => props.theme.colors.subtitle};
  margin: 10px 0;
`;

export const BackgroundContainer = styled.div`
  position: absolute;
  height: ${props => {
    return props.height === 'fixed' ? '80vh' : props.height === 'auto' ? 'auto' : props.height;
  }};
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const BackgroundImage = styled.img`
  width: 100%;
  top: 0;
  left: 0;
  z-index: -2;
  height: 100%;
`;

export const Veil = styled.div`
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  z-index: -1;
  background: black;
  height: 100%;
  opacity: ${props => props.opacity};
`;
