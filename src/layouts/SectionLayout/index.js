import React from 'react';
import PropTypes from 'prop-types';
import { StyledComponent, Container, Title, Description, BackgroundContainer, BackgroundImage, Veil } from './styles';

const IDBase = 'SectionLayout';
//Layout for Page
export const SectionLayout = ({
  children,
  id = '',
  height = 'fixed',
  justify = 'center',
  align = 'center',
  title = null,
  description = null,
  backgroundImageStyle = 'full',
  backgroundImage = null,
  opacity = null,
  order = '0',
}) => {
  return (
    <StyledComponent
      id={`${IDBase}${id}`}
      justify={justify}
      align={align}
      height={height}
      opacity={opacity}
      order={order}
    >
      {(!!title || !!description) && (
        <Container id={`${IDBase}Container`}>
          {!!title && <Title id={`${IDBase}Container__Title`}>{title}</Title>}
          {!!description && <Description id={`${IDBase}Container__Description`}>{description}</Description>}
        </Container>
      )}
      {!!backgroundImage && (
        <BackgroundContainer height={height}>
          <BackgroundImage src={backgroundImage} imageStyle={backgroundImageStyle} />
          {!!opacity && <Veil id="veil" opacity={opacity} />}
        </BackgroundContainer>
      )}
      {children}
    </StyledComponent>
  );
};

//
SectionLayout.propTypes = {
  children: PropTypes.node,
  justify: PropTypes.oneOf([null, '', 'center', 'top', 'bottom']),
  align: PropTypes.oneOf([null, '', 'left', 'center', 'right']),
  height: PropTypes.string, //Auto, Full, or Any
  title: PropTypes.string,
  description: PropTypes.string,
  opacity: PropTypes.string,
};

export default SectionLayout;
