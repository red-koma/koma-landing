import React from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  SocialContainer,
  SocialNetwork,
  FooterContainer,
  List,
  Title,
  SuscribeContainer,
  InputContainer,
  Description,
  LinkText,
} from './styles';
import InputText from '../../components/inputs/InputText';
import Button from '../../components/buttons/Button';
import Link from '../../components/buttons/Link';
import { useLanguageContext } from '../../contexts/LanguageContext';
import SocialButton from './../../components/buttons/SocialButton';
import instagram from './../../assets/images/social/instagram.png';
import facebook from './../../assets/images/social/facebook.png';
import twitter from './../../assets/images/social/twitter.png';

const IDBase = 'FooterLayout';
//Layout for Page
export const FooterLayout = ({ children, id = IDBase }) => {
  const { translate } = useLanguageContext();

  const handleLink = () => {};

  return (
    <>
      <Container id={`${IDBase}${id}`}>
        <SocialContainer>
          <SocialButton uri={instagram} />
          <SocialButton uri={facebook} />
          <SocialButton uri={twitter} />
        </SocialContainer>
      </Container>
      <FooterContainer>
        <List>
          <Title>{translate('CATEGORIES')}</Title>
          <Link onClick={() => handleLink()}>
            <LinkText>- {translate('TERRAINS')}</LinkText>
          </Link>
          <Link onClick={() => handleLink()}>
            <LinkText>- {translate('FABRICS')}</LinkText>
          </Link>
          <Link onClick={() => handleLink()}>
            <LinkText>- {translate('LOCALS')}</LinkText>
          </Link>
          <Link onClick={() => handleLink()}>
            <LinkText>- {translate('WITH-BEACH')}</LinkText>
          </Link>
          <Link onClick={() => handleLink()}>
            <LinkText>- {translate('MORE')}...</LinkText>
          </Link>
        </List>
        <List>
          <Title>{translate('CONTACT')}</Title>
          <Link onClick={() => handleLink()}>
            <LinkText>- 123 Calle Falsa numero, Santo Domingo</LinkText>
          </Link>
          <Link onClick={() => handleLink()}>
            <LinkText>- (809)123-4567</LinkText>
          </Link>
          <Link onClick={() => handleLink()}>
            <LinkText>- info@redkoma.com</LinkText>
          </Link>
        </List>
        <SuscribeContainer>
          <Title>{translate('LET-US-WRITE-CALL')}</Title>
          <InputContainer>
            <InputText />
            <Button onClick={() => console.log('Clicked')}>{translate('WRITE-CALL')}</Button>
          </InputContainer>
          <Description>{translate('FOOTER-DESCRIPTION')}</Description>
        </SuscribeContainer>
      </FooterContainer>
    </>
  );
};

//
FooterLayout.propTypes = {
  children: PropTypes.node,
};

export default FooterLayout;
