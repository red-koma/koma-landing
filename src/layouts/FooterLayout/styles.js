import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 5vh 0px;
  background-color: ${props => props.theme.colors.formBackground};
`;

export const SocialContainer = styled.section`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const SocialNetwork = styled.div`
  background: cyan;
  width: 10vw;
  height: 10vw;
  margin: 0 2vw;
`;

export const FooterContainer = styled.div`
  background-color: black;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  padding: 3vh 0;
`;

export const List = styled.div`
  width: 20%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

export const LinkText = styled.span`
  color: white;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.footerLink};
  text-align: left;
  margin-top: 0.5vh;
`;

export const Title = styled.span`
  width: 100%;
  font-weight: 700;
  text-align: left;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.footerTitle};
  color: ${props => props.theme.colors.white};
  margin-bottom: 1vh;
`;

export const Description = styled.span`
  width: 100%
  font-weight: 700;
  text-align: center;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.footerDescription};
  color: ${props => props.theme.colors.white};
  text-align: left;
`;

export const SuscribeContainer = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`;

export const InputContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;

export const TitleContainer = styled.div`
  width: 100%;
`;
