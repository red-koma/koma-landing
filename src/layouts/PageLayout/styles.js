import styled from 'styled-components';

//Apply Styled Component.
export const StyledComponent = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;
