import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import constants from '../../data/constants';
import { StyledComponent } from './styles';
import FooterLayout from '../FooterLayout';

//Layout for Page
export const PageLayout = ({ children, id = '', metaTitle, metaDescription = constants.PAGE_DESCRIPTION, styles }) => {
  return (
    <StyledComponent id={`Page__${id}`} styles={styles}>
      <Helmet>
        {metaTitle && <title>{`${metaTitle} | ${constants.PAGE_DESCRIPTION}`}</title>}
        {metaDescription && <meta name="description" content={metaDescription} />}
      </Helmet>
      {children}
      <FooterLayout />
    </StyledComponent>
  );
};

//
PageLayout.propTypes = {
  children: PropTypes.node,
  metaTitle: PropTypes.string,
  metaDescription: PropTypes.string,
};

export default PageLayout;
