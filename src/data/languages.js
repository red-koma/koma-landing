export const languages = [
  {
    id: 'es',
    tag: 'spanish',
    label: 'LANGUAGE.SPANISH',
    short: 'spa',
    abrev: 'sp',
    icon: 'ES',
    flag: require('../assets/images/svg/016-spain.svg'),
    countryCode: 'ES',
  },
  {
    id: 'en',
    tag: 'english',
    label: 'LANGUAGE.ENGLISH',
    short: 'eng',
    abrev: 'en',
    icon: 'US',
    flag: require('../assets/images/svg/226-united-states.svg'),
    countryCode: 'US',
  },
];

export const dataLanguage = {
  es: languages[0],
  en: languages[1],
  //   fr: languages[2],
  //   pr: languages[3],
};
