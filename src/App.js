import React from 'react';
import Routes from './pages/Routes';

export const App = () => {
  return (
    <React.Suspense fallback={null}>
      <Routes />
    </React.Suspense>
  );
};

export default App;
