import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, select } from '@storybook/addon-knobs';
import { Title, Subtitle, Description, Primary, Props } from '@storybook/addon-docs/blocks';
import { withA11y } from '@storybook/addon-a11y';
import { withLinks } from '@storybook/addon-links';
import { withPlayroom } from 'storybook-addon-playroom';
// import { withThemePlayground } from 'storybook-addon-theme-playground';
// import { ThemeProvider } from '@material-ui/core';
// import theme from '../../../styles/themes/theme';
import Component from './index';

export default {
  title: 'TextField',
  component: Component,
  decorators: [withKnobs, withA11y, withLinks, withPlayroom],
  parameters: {
    playroom: {
      url: 'http://localhost:9000',
    },
    docs: {
      page: () => (
        <>
          <Title>TextField</Title>
          <Subtitle>Basic Input</Subtitle>
          <Description>Input with 5 types of size, and with theme included.</Description>
          <Primary />
          <Props />
        </>
      ),
    },
  },
};

export const TextField = () => (
  <Component
    sizes={select('Sizes', ['normal', 'small', 'large', 'xlarge', 'full'], 'normal', 'prop.size')}
    onClick={action('clicked')}
    // variant = {select('Variant',['standard', 'outlined', 'filled'], 'outlined', 'Input')}
    label={text('Label', 'Label')}
  ></Component>
);

TextField.story = {
  name: 'Input',
  parameters: {
    playroom: {
      // Links addon doesn't work in Playroom, so display an alert instead
      code: "<TextField onClick={console.log('Test')}></TextField>",
      url: 'http://localhost:9000',
    },
  },
};
