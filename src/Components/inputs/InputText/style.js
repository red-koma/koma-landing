import styled from 'styled-components';
import TextField from '@material-ui/core/TextField';

export const StyledComponent = styled(TextField)`
  border-radius: ${props => props.theme.radius.common};
  background-color: ${props => props.theme.colors.inputBackground};
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.input};
  width: ${props => {
    switch (props.sizes) {
      case 'small':
        return props.theme.metrics.widths.small;
      case 'large':
        return props.theme.metrics.widths.large;
      case 'xlarge':
        return props.theme.metrics.widths.xlarge;
      case 'full':
        return props.theme.metrics.widths.full;

      default:
        return props.theme.metrics.widths.default;
    }
  }};
`;

export default StyledComponent;
