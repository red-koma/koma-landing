import React from 'react';
import PropTypes from 'prop-types';
import { StylesProvider } from '@material-ui/core/styles';
import StyledComponent from './style';

export const InputText = ({ sizes = 'normal', onClick, label, placeholder, ...props }) => {
  return (
    <StylesProvider injectFirst>
      <StyledComponent
        sizes={sizes}
        onClick={onClick}
        label={label}
        placeholder={placeholder}
        variant="outlined"
        size="small"
        {...props}
      />
    </StylesProvider>
  );
};

InputText.prototype = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  onClick: PropTypes.func,
  widths: PropTypes.oneOf([undefined, null, '', 'small', 'large', 'xlarge', 'full']),
};

export default InputText;
