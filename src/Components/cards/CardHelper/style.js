import styled from 'styled-components';

export const Container = styled.div`
  width: 15vw;
  height: 10vw;
  background-color: ${props => props.theme.colors.black};
  margin: 2vh 1vw;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;
  color: ${props => props.theme.colors.white};
`;

export const Label = styled.span`
  color: ${props => props.theme.colors.white};
  font-family: ${props => props.theme.fonts[0]};
  text-transform: uppercase;
  font-size: ${props => props.theme.fontSizes.xsmall};
`;
