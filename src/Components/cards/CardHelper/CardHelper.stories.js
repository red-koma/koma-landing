import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs } from '@storybook/addon-knobs';
import { Title, Subtitle, Description, Primary, Props } from '@storybook/addon-docs/blocks';
import { withA11y } from '@storybook/addon-a11y';
import { withLinks } from '@storybook/addon-links';
import Component from './index';
import { ThemeProvider } from 'styled-components';
import theme from './../../../styles/themes/StyledTheme';
import InfoIcon from '@material-ui/icons/Info';

export default {
  title: 'CardHelper',
  component: Component,
  decorators: [withKnobs, withA11y, withLinks],
  parameters: {
    docs: {
      page: () => (
        <>
          <Title>CardHelper</Title>
          <Subtitle>Basic CardHelper</Subtitle>
          <Description>Card to show helpers</Description>
          <Primary />
          <Props />
        </>
      ),
    },
  },
};

export const CardHelper = () => (
  <ThemeProvider theme={theme}>
    <Component onClick={action('clicked')}>{<InfoIcon fontSize="large" />}</Component>
  </ThemeProvider>
);

CardHelper.story = {
  name: 'CardHelper',
  parameters: {},
};
