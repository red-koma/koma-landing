import React from 'react';
import PropTypes from 'prop-types';
import { Container, Label } from './style';

export const CardHelper = ({ label = 'Comprar una propiedad', onClick, children }) => {
  return (
    <Container onClick={onClick}>
      {children}
      <Label>{label}</Label>
    </Container>
  );
};

CardHelper.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.node,
};

export default CardHelper;
