import React from 'react';
import PropTypes from 'prop-types';
import { StylesProvider } from '@material-ui/core/styles';
import StyledComponent, { LabelComponent } from './style';

const img = 'https://marcianosmx.com/wp-content/uploads/2013/08/minimoo64_fractals_01.jpg';

export const CardCategory = ({ size, image = img, children, onClick }) => {
  return (
    <StylesProvider injectFirst>
      <StyledComponent size={size} image={image} onClick={onClick}>
        <LabelComponent>{children}</LabelComponent>
      </StyledComponent>
    </StylesProvider>
  );
};

CardCategory.propTypes = {
  size: PropTypes.string,
  children: PropTypes.node.isRequired,
  image: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

export default CardCategory;
