import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import { Title, Subtitle, Description, Primary, Props } from '@storybook/addon-docs/blocks';
import { withA11y } from '@storybook/addon-a11y';
import { withLinks } from '@storybook/addon-links';
import Component from './index';
import { ThemeProvider } from 'styled-components';
import theme from '../../../styles/themes/StyledTheme';

export default {
  title: 'CardCategory',
  component: Component,
  decorators: [withKnobs, withA11y, withLinks],
  parameters: {
    docs: {
      page: () => (
        <>
          <Title>CardCategory</Title>
          <Subtitle>Basic CardCategory</Subtitle>
          <Description>Card to show categorys</Description>
          <Primary />
          <Props />
        </>
      ),
    },
  },
};

export const CardCategory = () => (
  <ThemeProvider theme={theme}>
    <Component onClick={action('clicked')}>{text('Label', 'Im a button')}</Component>
  </ThemeProvider>
);

CardCategory.story = {
  name: 'CardCategories',
  parameters: {},
};
