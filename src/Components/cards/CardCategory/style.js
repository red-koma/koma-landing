import styled from 'styled-components';
import InputLabel from '@material-ui/core/InputLabel';

export const StyledComponent = styled.div`
  background-image: url(${props => props.image});
  background-size: ${props => {
      switch (props.size) {
        case 'large':
          return '30vw';

        default:
          return '18vw';
      }
    }}
    15vw;
  width: ${props => {
    switch (props.size) {
      case 'large':
        return '30vw';

      default:
        return '18vw';
    }
  }};
  height: 15vw;
  margin: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const LabelComponent = styled(InputLabel)`
  && {
    font-weight: bold;
    font-size: ${props => props.theme.fontSizes.formTitle};
    color: ${props => props.theme.colors.white};
  }
`;

export default StyledComponent;
