import styled from 'styled-components';
import button from '@material-ui/core/Button';

export const Container = styled.section`
  background-image: url(${props => props.image});
  background-size: 17vw 60vh;
  width: 17vw;
  height: 60vh;
  margin: 5vh 5px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const ButtonContainer = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.span`
  width: 80%;
  font-weight: 700;
  text-align: left;
  color: ${props => props.theme.colors.white};
  margin-top: 10px;
  padding-left: 0.5vw;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.title};
`;

export const Button = styled(button)`
  && {
    background: ${props => props.theme.colors.none};
    width: 100%;
    justify-content: space-between;
    color: ${props => props.theme.colors.white};
    font-size: ${props => props.theme.fontSizes.small};
  }
`;
