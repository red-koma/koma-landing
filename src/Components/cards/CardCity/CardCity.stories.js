import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import { Title, Subtitle, Description, Primary, Props } from '@storybook/addon-docs/blocks';
import { withA11y } from '@storybook/addon-a11y';
import { withLinks } from '@storybook/addon-links';
import Component from './index';
import { ThemeProvider } from 'styled-components';
import theme from './../../../styles/themes/StyledTheme';

export default {
  title: 'CardCity',
  component: Component,
  decorators: [withKnobs, withA11y, withLinks],
  parameters: {
    docs: {
      page: () => (
        <>
          <Title>CardCity</Title>
          <Subtitle>Basic CardCity</Subtitle>
          <Description>Card to show cities</Description>
          <Primary />
          <Props />
        </>
      ),
    },
  },
};

export const CardCity = () => (
  <ThemeProvider theme={theme}>
    <Component onClick={action('clicked')}>{text('Label', 'Im a button')}</Component>
  </ThemeProvider>
);

CardCity.story = {
  name: 'CardCity',
  parameters: {},
};
