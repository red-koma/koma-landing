import React from 'react';
import PropTypes from 'prop-types';
import { StylesProvider } from '@material-ui/core/styles';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Container, Title, ButtonContainer, Button } from './style';

const img =
  'https://images.adsttc.com/media/images/55e6/3a00/8450/b5b8/8b00/02d2/slideshow/1282771419-exterior-01.jpg?1441151481';

export const CardCity = ({ image = img, title = 'Ciudad', onClick }) => {
  return (
    <StylesProvider injectFirst>
      <Container image={image} onClick={onClick}>
        <Title>{title}</Title>
        <ButtonContainer>
          <Button endIcon={<ArrowForwardIosIcon />}>Ver propiedades</Button>
        </ButtonContainer>
      </Container>
    </StylesProvider>
  );
};

CardCity.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

export default CardCity;
