import React from 'react';
// import { action } from '@storybook/addon-actions';
import { withKnobs } from '@storybook/addon-knobs';
import { Title, Subtitle, Description, Primary, Props } from '@storybook/addon-docs/blocks';
import { withA11y } from '@storybook/addon-a11y';
import { withLinks } from '@storybook/addon-links';

import Component from './index';
import { LanguageContextProvider } from '../../../contexts/LanguageContext';

export default {
  title: 'ButtonLanguage',
  component: Component,
  decorators: [withKnobs, withA11y, withLinks],
  parameters: {
    docs: {
      page: () => (
        <>
          <Title>BUTTON LANGUAGE</Title>
          <Subtitle>Button to change Language.</Subtitle>
          <Description>Button with menu modal to change language using language Context.</Description>
          <Primary />
          <Props />
        </>
      ),
    },
  },
};

export const ButtonLanguage = () => {
  return (
    <LanguageContextProvider>
      <Component />
    </LanguageContextProvider>
  );
};
