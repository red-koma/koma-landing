import React from 'react';

import { StylesProvider } from '@material-ui/core/styles';

import { useLanguageContext } from '../../../contexts/LanguageContext';

import { ActualText, ButtonL, Label, Container, DropdownMenuItem, DropdownMenu } from './styles';

const Options = ['PROFILE.PROFILE', 'LOGOUT'];

export const ButtonProfile = ({ username }) => {
  const { translate } = useLanguageContext();
  const [dropdownModal, setDropdownModal] = React.useState(null);

  //
  const handleClick = event => {
    setDropdownModal(event.currentTarget);
  };

  //
  const handleClose = () => {
    setDropdownModal(null);
  };

  //
  const selectOption = id => {
    setDropdownModal(null);
  };

  //
  const renderOptions = () => {
    return Options.map(item => {
      return (
        <DropdownMenuItem key={item} onClick={() => selectOption(item)}>
          <Label>{translate(item)}</Label>
        </DropdownMenuItem>
      );
    });
  };

  //
  return (
    <StylesProvider injectFirst>
      <Container id={'__Container'}>
        <ButtonL id={'__Container__ButtonL'} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
          <ActualText id={'__Container_ButtonL__ActualText'}>{username}</ActualText>
        </ButtonL>
        <DropdownMenu
          id={'__Container_DropdownMenu'}
          anchorEl={dropdownModal}
          keepMounted
          open={!!dropdownModal}
          onClose={() => handleClose()}
        >
          {renderOptions()}
        </DropdownMenu>
      </Container>
    </StylesProvider>
  );
};

export default ButtonProfile;
