import styled from 'styled-components';
import MUIButton from '@material-ui/core/Button';
import MUIDropdownMenu from '@material-ui/core/Menu';
import MUIDropdownMenuItem from '@material-ui/core/MenuItem';

export const Container = styled.div`
  display: flex;
  margin-right: 20px;
`;

export const DropdownMenu = styled(MUIDropdownMenu)`
  display: flex;
`;

export const DropdownMenuItem = styled(MUIDropdownMenuItem)`
  &&display: flex;
`;

export const ActualText = styled.span`
  color: ${props => props.theme.colors.textButton};
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.button};
`;

export const ButtonL = styled(MUIButton)`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.theme.colors.none};
  min-width: none;
  background: ${props => props.theme.colors.btnInverseBackground};

  padding & .MuiButton-label {
  }
`;

export const Label = styled.span`
  color: ${props => props.theme.colors.black};
  display: inline-flex;
  align-items: center;
  justify-content: flex-start;
  margin-left: 10px;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.small};
`;
