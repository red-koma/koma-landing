import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, select } from '@storybook/addon-knobs';
import { Title, Subtitle, Description, Primary, Props } from '@storybook/addon-docs/blocks';
import { withA11y } from '@storybook/addon-a11y';
import { withLinks } from '@storybook/addon-links';
import { withPlayroom } from 'storybook-addon-playroom';
import Component from './index';

export default {
  title: 'Button',
  component: Component,
  decorators: [withKnobs, withA11y, withLinks, withPlayroom],
  parameters: {
    playroom: {
      url: 'http://localhost:9000',
    },
    docs: {
      page: () => (
        <>
          <Title>BUTTON</Title>
          <Subtitle>Basic Text Button</Subtitle>
          <Description>Button with 5 types of size, and with theme included.</Description>
          <Primary />
          <Props />
        </>
      ),
    },
  },
};

export const Button = () => (
  <Component
    size={select('Sizes', ['normal', 'small', 'large', 'xlarge', 'full'], 'normal', 'prop.size')}
    onClick={action('clicked')}
  >
    {text('Label', 'Im a button')}
  </Component>
);

Button.story = {
  name: 'Button',
  parameters: {},
};
