import React from 'react';
import PropTypes from 'prop-types';
import { StylesProvider } from '@material-ui/core/styles';
import StyledComponent from './styles';

export const Link = ({ children, onClick }) => {
  return (
    <StylesProvider injectFirst>
      <StyledComponent onClick={onClick}>{children}</StyledComponent>
    </StylesProvider>
  );
};

Link.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func.isRequired,
};

export default Link;
