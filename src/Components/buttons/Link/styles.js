import styled from 'styled-components';
import MUIButton from '@material-ui/core/Button';

export const StyledComponent = styled(MUIButton)`
  background: ${props => props.theme.colors.none};
  border: 0;
  color: ${props => props.theme.colors.link};
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.link};
  padding: 0px 10px;
  }};
`;

export default StyledComponent;
