import React from 'react';
import PropTypes from 'prop-types';
import { StylesProvider } from '@material-ui/core/styles';
import StyledComponent from './styles';

export const Button = ({ children, size = 'medium', onClick }) => {
  return (
    <StylesProvider injectFirst>
      <StyledComponent size={size} onClick={onClick}>
        {children}
      </StyledComponent>
    </StylesProvider>
  );
};

Button.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func.isRequired,
  size: PropTypes.oneOf([undefined, null, '', 'small', 'large', 'xlarge', 'full']),
};

export default Button;
