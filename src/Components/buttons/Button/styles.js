import styled from 'styled-components';
import MUIButton from '@material-ui/core/Button';

export const StyledComponent = styled(MUIButton)`
  background: ${props => props.theme.colors.background};
  border: 0;
  border-radius: ${props => props.theme.radius.common};
  color: ${props => props.theme.colors.textButton};
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.button};
  height: 35px;
  margin: 10px;
  padding: 0px 10px;
  width: ${props => {
    switch (props.size) {
      case 'small':
        return props.theme.metrics.widths.small;
      case 'large':
        return props.theme.metrics.widths.large;
      case 'xlarge':
        return props.theme.metrics.widths.xlarge;
      case 'full':
        return props.theme.metrics.widths.full;

      default:
        return props.theme.metrics.widths.default;
    }
  }};
`;

export default StyledComponent;
