import styled from 'styled-components';
import MUIButton from '@material-ui/core/Button';

export const Container = styled.div``;

export const Img = styled.img`
  width: 35px;
  height: 35px;
`;

export const ActualText = styled.span`
  position: absolute;
  width: 35px;
  height: 35px;
  color: ${props => props.theme.colors.textButton};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.button};
`;

export const ButtonL = styled(MUIButton)`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 35px;
  height: 35px;

  background-color: ${props => props.theme.colors.none};
  min-width: auto;
  padding & .MuiButton-label {
    width: auto;
  }
`;

export const Label = styled.span`
  height: 35px;
  color: ${props => props.theme.colors.black};
  display: inline-flex;
  align-items: center;
  justify-content: flex-start;
  margin-left: 10px;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.small};
`;
