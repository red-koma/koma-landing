import React from 'react';
import DropdownMenu from '@material-ui/core/Menu';
import DropdownMenuItem from '@material-ui/core/MenuItem';
import { StylesProvider } from '@material-ui/core/styles';

import { useLanguageContext } from '../../../contexts/LanguageContext';
import { languages as RawLanguages } from '../../../data/languages';

import ImgSpain from '../../../assets/images/flags/spain.png';
import ImgUSA from '../../../assets/images/flags/usa.png';

import { Img, ActualText, ButtonL, Label, Container } from './styles';

export const ButtonLanguage = props => {
  const { language, translate, onChange } = useLanguageContext();
  const [dropdownModal, setDropdownModal] = React.useState(null);
  const [langObject, setLangObject] = React.useState(RawLanguages.find(x => x.id === language));

  const handleClick = event => {
    setDropdownModal(event.currentTarget);
  };

  const handleClose = () => {
    setDropdownModal(null);
  };

  const changeLanguage = id => {
    onChange(id);
    setLangObject(RawLanguages.find(x => x.id === id));
    setDropdownModal(null);
  };

  const renderLanguages = () => {
    return RawLanguages.map(item => {
      return (
        language !== item.id && (
          <DropdownMenuItem key={item.id} onClick={() => changeLanguage(item.id)}>
            <Img src={item.id === 'es' ? ImgSpain : ImgUSA} alt={'language-selected-' + langObject.tag} />
            <Label>{translate(item.label)}</Label>
          </DropdownMenuItem>
        )
      );
    });
  };

  return (
    <StylesProvider injectFirst>
      <Container>
        <ButtonL aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
          <Img src={language === 'es' ? ImgSpain : ImgUSA} alt={'language-selected-' + langObject.tag} />
          <ActualText>{language.toUpperCase()}</ActualText>
        </ButtonL>
        <DropdownMenu anchorEl={dropdownModal} keepMounted open={!!dropdownModal} onClose={() => handleClose()}>
          {renderLanguages()}
        </DropdownMenu>
      </Container>
    </StylesProvider>
  );
};

export default ButtonLanguage;
