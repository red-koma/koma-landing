import React from 'react';
import PropTypes from 'prop-types';
import { Container, Image } from './style';

export const SocialButton = ({ onClick, uri }) => {
  return (
    <Container onClick={onClick}>
      <Image src={uri} alt="imagen" width="100%" />
    </Container>
  );
};

SocialButton.propTypes = {
  onClick: PropTypes.func,
  image: PropTypes.string.isRequired,
};

export default SocialButton;
