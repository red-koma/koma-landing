import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs } from '@storybook/addon-knobs';
import { Title, Subtitle, Description, Primary, Props } from '@storybook/addon-docs/blocks';
import { withA11y } from '@storybook/addon-a11y';
import { withLinks } from '@storybook/addon-links';
import Component from './index';
import { ThemeProvider } from 'styled-components';
import theme from '../../../styles/themes/StyledTheme';

export default {
  title: 'SocialButton',
  component: Component,
  decorators: [withKnobs, withA11y, withLinks],
  parameters: {
    docs: {
      page: () => (
        <>
          <Title>SocialButton</Title>
          <Subtitle>Basic SocialButton</Subtitle>
          <Description>Button to show Social Networks</Description>
          <Primary />
          <Props />
        </>
      ),
    },
  },
};

export const SocialButton = () => (
  <ThemeProvider theme={theme}>
    <Component onClick={action('clicked')}></Component>
  </ThemeProvider>
);

SocialButton.story = {
  name: 'SocialButton',
  parameters: {},
};
