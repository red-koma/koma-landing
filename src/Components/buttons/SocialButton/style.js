import styled from 'styled-components';

export const Container = styled.div`
  width: 10vw;
  height: 10vw;
  border-radius: 100%;
  margin: 0 2vw;
`;

export const Image = styled.img`
  width: 100%;
  border-radius: 100%;
`;
