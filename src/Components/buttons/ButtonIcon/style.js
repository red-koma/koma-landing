import styled from 'styled-components';
import ButtonIcon from '@material-ui/core/IconButton';

export const StyledComponent = styled(ButtonIcon)`
  background: ${props => (props.background ? props.background : props.theme.colors.background)};
  border-radius: ${props => props.theme.radius.common};
  color: ${props => (props.colored ? props.colored : props.theme.colors.icon)};
  height: 40px;
  margin: 10px;
`;
export default StyledComponent;
