import React from 'react';
import PropTypes from 'prop-types';
import { StylesProvider } from '@material-ui/core/styles';
import StyledComponent from './style';

export const ButtonIcon = ({ children, onClick, color = 'white', background = 'black' }) => {
  return (
    <StylesProvider injectFirst>
      <StyledComponent colored={color} background={background} onClick={onClick}>
        {children}
      </StyledComponent>
    </StylesProvider>
  );
};

ButtonIcon.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  color: PropTypes.string,
  background: PropTypes.string,
};

export default ButtonIcon;
