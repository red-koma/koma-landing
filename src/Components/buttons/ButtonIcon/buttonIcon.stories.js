import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, select } from '@storybook/addon-knobs';
import { Title, Subtitle, Description, Primary, Props } from '@storybook/addon-docs/blocks';
import { withA11y } from '@storybook/addon-a11y';
import { withLinks } from '@storybook/addon-links';
import { withPlayroom } from 'storybook-addon-playroom';
// import { withThemePlayground } from 'storybook-addon-theme-playground';
// import { ThemeProvider } from '@material-ui/core';
// import theme from '../../../styles/themes/theme';
import Component from './index';

export default {
    title: 'ButtonIcon',
    component: Component,
    decorators: [
      withKnobs,
      withA11y,
      withLinks,
      withPlayroom,
      // withThemePlayground({
      //   theme,
      //   provider: ThemeProvider,
      // }),
    ],
    parameters: {
      playroom: {
        url: 'http://localhost:9000',
      },
      docs: {
        page: () => (
          <>
            <Title>BUTTONICON</Title>
            <Subtitle>Basic icon Button</Subtitle>
            <Description>Button with icon.</Description>
            <Primary />
            <Props />
          </>
        ),
      },
    },
  };
  
  export const ButtonIcon = () => (
    <Component
      onClick={action('clicked')}
    >
      {text('Label', 'IC')}
    </Component>
  );
  
  ButtonIcon.story = {
    name: 'ButtonIcon',
    parameters: {
      playroom: {
        // Links addon doesn't work in Playroom, so display an alert instead
        code: "<Button onClick={console.log('Test')}></Button>",
        url: 'http://localhost:9000',
      },
    },
  };
  