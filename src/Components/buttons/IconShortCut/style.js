import styled from 'styled-components';

export const StyledComponent = styled.div`
  background-color: rgba(255, 255, 255, 0.37);
  width: 5vw;
  height: 5vw;
`;

export const ImageComponent = styled.img`
  width: 100%;
`;

export default StyledComponent;
