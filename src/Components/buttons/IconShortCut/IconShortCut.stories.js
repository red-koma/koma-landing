import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs } from '@storybook/addon-knobs';
import { Title, Subtitle, Description, Primary, Props } from '@storybook/addon-docs/blocks';
import { withA11y } from '@storybook/addon-a11y';
import { withLinks } from '@storybook/addon-links';
import { withPlayroom } from 'storybook-addon-playroom';
import Component from './index';

export default {
  title: 'IconShortCut',
  component: Component,
  decorators: [withKnobs, withA11y, withLinks, withPlayroom],
  parameters: {
    docs: {
      page: () => (
        <>
          <Title>IconShortCut</Title>
          <Subtitle>IconShortCut basic</Subtitle>
          <Description>Element recive a image png.</Description>
          <Primary />
          <Props />
        </>
      ),
    },
  },
};

export const IconShortCut = () => <Component onClick={action('clicked')}></Component>;

IconShortCut.story = {
  name: 'IconShortCut',
  parameters: {},
};
