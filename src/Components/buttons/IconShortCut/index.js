import React from 'react';
import PropTypes from 'prop-types';
import { StylesProvider } from '@material-ui/core/styles';
import { StyledComponent, ImageComponent } from './style';

const img = 'https://cdn.icon-icons.com/icons2/1522/PNG/512/homelinear_106213.png';

export const IconShortCut = ({ image = img, onClick }) => {
  return (
    <StylesProvider injectFirst>
      <StyledComponent>
        <ImageComponent src={image} onClick={onClick} />
      </StyledComponent>
    </StylesProvider>
  );
};

IconShortCut.propTypes = {
  image: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

export default IconShortCut;
