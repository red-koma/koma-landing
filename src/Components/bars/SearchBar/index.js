import React from 'react';
import ButtonIcon from '../../buttons/ButtonIcon';
import Input from '../../inputs/InputText';
import SearchIcon from '@material-ui/icons/Search';
import { StylesProvider } from '@material-ui/core/styles';
import StyledComponent from './style';
import { useLanguageContext } from '../../../contexts/LanguageContext';

export const SearchBar = () => {
  const { translate } = useLanguageContext();

  return (
    <StylesProvider>
      <StyledComponent>
        <Input placeholder={translate('SEARCH')} sizes={'large'} />
        <ButtonIcon>
          <SearchIcon />
        </ButtonIcon>
      </StyledComponent>
    </StylesProvider>
  );
};

export default SearchBar;
