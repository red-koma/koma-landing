const theme = {
  colors: {
    none: 'transparent',
    app: '#000000',
    white: '#FFFFFF',
    black: 'black',
    icon: '#FFFFFF',
    gray: '#AFAFAF',
    background: 'linear-gradient(45deg, #0a0a0a 30%, #000000 90%)',
    textButton: '#FFFFFF',
    inputBackground: '#FFFFFF',
    title: 'black',
    subtitle: 'gray',
    link: 'blue',
    formBackground: '#C4C4C4',
    btnInverseBackground: 'rgba(0,0,0,0.3)',
    error: 'red',
  },
  fonts: ['sans-serif', 'Roboto'],
  fontSizes: {
    xsmall: '0.8em',
    small: '1em',
    medium: '2em',
    large: '3em',
    title: '2em',
    subtitle: '0.8em',
    input: '2em',
    button: '1em',
    formTitle: '1.5em',
    formSubtitle: '0.9em',
    h0: '2.5em',
    h2: '1.5em',
    h3: '1.2em',
    footerTitle: '1em',
    footerDescription: '0.7em',
    footerLink: '0.8em',
    error: '0.85em',
  },
  radius: {
    common: '5px',
  },

  metrics: {
    widths: {
      small: '150px',
      large: '400px',
      xlarge: '650px',
      full: '80vw',
      default: '250px',
    },
  },
};

export default theme;
