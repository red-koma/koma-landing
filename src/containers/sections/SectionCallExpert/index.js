import React from 'react';
import { TextContainer, Title, Description } from './styles';
import SectionLayout from '../../../layouts/SectionLayout';

import { useLanguageContext } from '../../../contexts/LanguageContext';
import { Button } from '../../../components/buttons/Button';

const IMAGE = 'https://flconsultants.fr/wp-content/uploads/2016/12/ReunionConseilOriginal-1170x570.jpeg';

export const SectionCallExpert = () => {
  const { translate } = useLanguageContext();

  return (
    <SectionLayout
      id="SectionExpert__"
      justify="center"
      align="left"
      opacity="0.4"
      backgroundImage={IMAGE}
      height="auto"
      order="-3"
    >
      <TextContainer>
        <Title>{translate('SECTION-EXPERT-TITLE')}</Title>
        <Description>{translate('SECTION-EXPERT-DESCRIPTION')}</Description>
        <Button
          onClick={() => {
            console.log('presses');
          }}
        >
          {translate('SECTION-EXPERT-SPEAK-TO-EXPERT')}
        </Button>
      </TextContainer>
    </SectionLayout>
  );
};

export default SectionCallExpert;
