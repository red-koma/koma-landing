import styled from 'styled-components';

export const TextContainer = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: left;
  margin: 15vh 0 15vh 5vw;
`;

export const Title = styled.span`
  width: ${props => props.theme.metrics.widths.large};
  font-weight: 700;
  text-align: left;
  color: ${props => props.theme.colors.title};
  margin-bottom: 10px;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.h0};
  margin: 0 0 20px 10px;
`;

export const Description = styled.span`
  width: 80%;
  font-size: 14px;
  font-weight: 400;
  text-align: left;
  color: ${props => props.theme.colors.black};
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.h3};
  margin: 0 0 10px 10px;
`;
