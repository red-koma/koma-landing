import styled from 'styled-components';

export const Container = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const TextContainer = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 17vw;
`;

export const City = styled.div`
  width: 17vw;
  height: 60vh;
  background-color: cyan;
  margin: 5vh 5px;
`;

export const Title = styled.span`
  width: 80%;
  font-weight: 700;
  text-align: left;
  color: ${props => props.theme.colors.title};
  margin-bottom: 10px;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.title};
`;

export const Description = styled.span`
  width: 80%;
  font-size: 14px;
  font-weight: 400;
  text-align: left;
  color: ${props => props.theme.colors.subtitle};
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.subtitle};
`;
