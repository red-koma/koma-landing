import React from 'react';
import { Container, TextContainer, Title, Description, City } from './styles';
import SectionLayout from '../../../layouts/SectionLayout';
import CardCity from './../../../components/cards/CardCity';
import { useLanguageContext } from '../../../contexts/LanguageContext';

export const SectionCities = () => {
  const { translate } = useLanguageContext();

  return (
    <SectionLayout id="SectionCities__" justify="top" align="center" height="auto" orientation="row">
      <Container>
        <TextContainer>
          <Title>{translate('SECTION-CITIES-TITLE')}</Title>
          <Description>{translate('SECTION-CITIES-DESCRIPTION')}</Description>
        </TextContainer>
        <CardCity />
        <City />
        <City />
        <City />
      </Container>
    </SectionLayout>
  );
};

export default SectionCities;
