import React from 'react';
import { Container, Title, Action } from './styles';
import SectionLayout from '../../../layouts/SectionLayout';

import { useLanguageContext } from '../../../contexts/LanguageContext';

const IMAGE = 'https://media-cdn.tripadvisor.com/media/photo-s/11/4c/79/49/mar-playa-y-cormorant.jpg';

export const SectionCategories = () => {
  const { translate } = useLanguageContext();

  return (
    <SectionLayout
      id="SectionCategories__"
      justify="center"
      align="center"
      height="fixed"
      opacity="0.4"
      backgroundImage={IMAGE}
      backgroundImageStyle="diagonal"
    >
      <Title>{translate('SECTION-ACTIONS-TITLE')}</Title>
      <Container>
        <Action />
        <Action />
        <Action />
        <Action />
      </Container>
    </SectionLayout>
  );
};

export default SectionCategories;
