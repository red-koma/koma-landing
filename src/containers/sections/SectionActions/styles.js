import styled from 'styled-components';

export const Container = styled.section`
  width: 100%;

  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-bottom: 5vh;
  flex-wrap: wrap;
  padding: 2vh 5vw;
`;

export const Title = styled.span`
  width: 70%;
  font-family: ${props => props.theme.fonts[0]};
  font-size: ${props => props.theme.fontSizes.h2};
  color: ${props => props.theme.colors.white};
  text-align: center;
`;

export const Action = styled.div`
  width: 15vw;
  height: 10vw;
  background-color: cyan;
  margin: 2vh 1vw;
`;
