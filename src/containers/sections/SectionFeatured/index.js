import React from 'react';
import { Container, Featured } from './styles';
import SectionLayout from '../../../layouts/SectionLayout';

import { useLanguageContext } from '../../../contexts/LanguageContext';

export const SectionFeatured = () => {
  const { translate } = useLanguageContext();

  return (
    <SectionLayout
      id="SectionFeatured__"
      justify="top"
      align="center"
      height="auto"
      title={translate('SECTION-FEATURED-TITLE')}
      description={translate('SECTION-FEATURED-DESCRIPTION')}
    >
      <Container>
        <Featured />
        <Featured />
        <Featured />
        <Featured />
        <Featured />
        <Featured />
      </Container>
    </SectionLayout>
  );
};

export default SectionFeatured;
