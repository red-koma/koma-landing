import styled from 'styled-components';

export const Container = styled.section`
  width: 100%;
  display: grid;

  grid-gap: 1vw;
  grid-template-columns: auto auto auto;
  justify-content: center;
  align-items: center;
  margin-bottom: 5vh;
`;

export const Featured = styled.div`
  width: 25vw;
  height: 20vw;
  background-color: cyan;
`;
