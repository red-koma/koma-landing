import React from 'react';
import { Header, HeaderRight, Logo, SearchBarContainer, IconsContainer, IconItem } from './styles';
import SectionLayout from '../../../layouts/SectionLayout';
import Button from '../../../components/buttons/Button';
import ButtonProfile from '../../../components/buttons/ButtonProfile';

import ButtonLanguage from '../../../components/buttons/ButtonLanguage';
import SearchBar from '../../../components/bars/SearchBar';
import { useLanguageContext } from '../../../contexts/LanguageContext';
import { useAuthContext } from '../../../contexts/AuthContext';

const IMAGE = 'https://www.dlfcrest.org.in/wp-content/uploads/2015/01/featured.png';

export const SectionLanding = onLogin => {
  const { translate } = useLanguageContext();
  const { isAuthenticated, username } = useAuthContext();
  // const username = 'Edison Sanchez';

  console.log('Is Authenticated? ', isAuthenticated);
  //
  return (
    <SectionLayout justify="top" align="center" opacity="0.2" backgroundImage={IMAGE}>
      <Header id={'SectionLayout-Header'}>
        <Logo id={'SectionLayout-Header-Logo'} />
        <HeaderRight id={'SectionLayout-RightContainer__'}>
          <ButtonLanguage id={'SectionLayout-Header-Language'} />
          {!isAuthenticated && (
            <Button size="small" id={'SectionLayout-Header-Button'} onClick={onLogin()}>
              {translate('LOGIN')}
            </Button>
          )}
          {!!isAuthenticated && <ButtonProfile id="ButtonProfile__" username={username} />}
        </HeaderRight>
      </Header>
      <SearchBarContainer id="SearchBarcontainer">
        <SearchBar />
      </SearchBarContainer>
      <IconsContainer>
        <IconItem src="https://library.kissclipart.com/20191026/bre/kissclipart-essential-compilation-icon-house-icon-75f3040892972b6b.png" />
        <IconItem src="https://library.kissclipart.com/20191026/bre/kissclipart-essential-compilation-icon-house-icon-75f3040892972b6b.png" />
        <IconItem src="https://library.kissclipart.com/20191026/bre/kissclipart-essential-compilation-icon-house-icon-75f3040892972b6b.png" />
        <IconItem src="https://library.kissclipart.com/20191026/bre/kissclipart-essential-compilation-icon-house-icon-75f3040892972b6b.png" />
      </IconsContainer>
    </SectionLayout>
  );
};

SectionLanding.propTypes = {};

export default SectionLanding;
