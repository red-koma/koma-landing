import styled from 'styled-components';

export const BackgroundContainer = styled.div`
  position: absolute;
  height: 80vh;
  width: 100%;
`;

export const BackgroundImage = styled.img`
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  z-index: -1;
  height: 80vh;
`;

export const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

export const HeaderRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const Logo = styled.div`
  width: 100px;
  height: 50px;
  margin: 10px;
  background-color: yellow;
`;

export const SearchBarContainer = styled.div`
  width: 100%;
  height: 60vh;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
`;

export const SearchBarComponent = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  ${'' /* background-color: gold; */}
`;

export const SearchBar = styled.div`
  width: 30%;
  height: 40px;
  background-color: ${props => props.theme.colors.white};
`;

export const IconsContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const IconItem = styled.img`
  width: 50px;
  height: 50px;
  margin: 10px 20px;
`;
