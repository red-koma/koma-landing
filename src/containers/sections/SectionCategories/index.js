import React from 'react';
import { Container } from './styles';
import SectionLayout from '../../../layouts/SectionLayout';
import CardCategory from '../../../components/cards/CardCategory';

import { useLanguageContext } from '../../../contexts/LanguageContext';

//TODO Remove IMAGE DEFAULT.
const IMAGE = 'https://gramor.com/wp-content/uploads/2013/03/205-Place2_leasing_lg.jpg';

export const SectionCategories = () => {
  const { translate } = useLanguageContext();

  return (
    <SectionLayout
      id="SectionCategories__"
      justify="top"
      align="center"
      height="auto"
      title={translate('SECTION-CATEGORIES-TITLE')}
      description={translate('SECTION-CATEGORIES-DESCRIPTION')}
    >
      <Container>
        <CardCategory size={'large'} image={IMAGE}>
          one
        </CardCategory>
        <CardCategory image={IMAGE}>one</CardCategory>
        <CardCategory image={IMAGE}>one</CardCategory>
        <CardCategory image={IMAGE}>one</CardCategory>

        <CardCategory image={IMAGE}>one</CardCategory>
        <CardCategory image={IMAGE}>one</CardCategory>
        <CardCategory size={'large'} image={IMAGE}>
          one
        </CardCategory>
        <CardCategory image={IMAGE}>one</CardCategory>
      </Container>
    </SectionLayout>
  );
};

export default SectionCategories;
