import styled from 'styled-components';

export const Container = styled.section`
  width: 100%;

  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-bottom: 5vh;
  flex-wrap: wrap;
  padding: 2vh 5vw;
`;

export const CategoryLarge = styled.div`
  width: 30vw;
  height: 15vw;
  background-color: cyan;
  margin: 5px;
`;

export const Category = styled.div`
  width: 18vw;
  height: 15vw;
  background-color: cyan;
  margin: 5px;
`;
