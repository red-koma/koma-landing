import React from 'react';

import InputText from '../../../components/inputs/InputText';
import Link from '../../../components/buttons/Link';
import Button from '../../../components/buttons/Button';
import CloseIcon from '@material-ui/icons/Close';
import { useLanguageContext } from '../../../contexts/LanguageContext';
import {
  Container,
  Header,
  Body,
  Title,
  Subtitle,
  InputContainer,
  ButtonContainer,
  RightContainer,
  LinkContainer,
} from './styles';

export const Login = () => {
  const { translate } = useLanguageContext();
  return (
    <Container>
      <Header>
        <RightContainer>
          <CloseIcon />
        </RightContainer>
        <Title>{translate('CONFIRM')}</Title>
        <Subtitle>{translate('CONFIRM-SUBTITLE')}</Subtitle>
      </Header>
      <Body>
        <InputContainer>
          <InputText sizes="large" placeholder={translate('EMAIL')} />
        </InputContainer>
        <LinkContainer>
          <Link>{translate('CONFIRM-NOT-RECEIVED')}</Link>
        </LinkContainer>
        <ButtonContainer>
          <Button size="large">{translate('LOGIN')}</Button>
        </ButtonContainer>
      </Body>
    </Container>
  );
};

export default Login;
