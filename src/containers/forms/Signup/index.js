import React from 'react';
import CloseIcon from '@material-ui/icons/Close';
import Button from '../../../components/buttons/Button';
import Input from '../../../components/inputs/InputText';
import StyledComponent, { DivComponent, LabelComponent, IconComponent } from './style';
import Buttonicon from '../../../components/buttons/ButtonIcon';
import { useLanguageContext } from '../../../contexts/LanguageContext';
import { useAuthContext } from '../../../contexts/AuthContext';
import useInputValue from '../../../hooks/useInputValue';

export const Signup = ({ username = '', onSubmit, onClose }) => {
  const { translate } = useLanguageContext();
  const { authSignup } = useAuthContext();

  const _username = useInputValue(username, 'email', translate('LOGIN.USERNAME'));
  const _password = useInputValue('', 'password', translate('LOGIN.PASSWORD'));
  const _confirm_password = useInputValue('', 'confirm', translate('LOGIN.CONFIRM'));

  //
  const handleSubmit = async () => {
    const response = await authSignup(_username, _password);

    if (response.data) {
      onSubmit(_username, _password);
    }
  };

  return (
    <StyledComponent>
      <IconComponent>
        <Buttonicon background={'transparent'} colors={'black'} onClick={onClose()}>
          <CloseIcon fontSize={'large'} />
        </Buttonicon>
      </IconComponent>
      <DivComponent>
        <LabelComponent>{translate('SIGNUP')}</LabelComponent>
      </DivComponent>
      <DivComponent>
        <Input sizes={'large'} {..._username} />
      </DivComponent>
      <DivComponent>
        <Input sizes={'large'} {..._password} />
      </DivComponent>
      <DivComponent>
        <Input sizes={'large'} {..._confirm_password} />
      </DivComponent>
      <DivComponent>
        <Button onClick={handleSubmit}>{translate('SIGNUP')}</Button>
      </DivComponent>
    </StyledComponent>
  );
};

export default Signup;
