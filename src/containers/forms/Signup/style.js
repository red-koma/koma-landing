import styled from 'styled-components';
import FormLabel from '@material-ui/core/FormLabel';

export const StyledComponent = styled.form`
  position: absolute;
  width: 35vw;
  height: 26vw;
  background: ${props => props.theme.colors.formBackground};
  box-shadow: 10px 10px 4px rgba(0, 0, 0, 0.2);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const DivComponent = styled.div`
  margin-top: 1vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const LabelComponent = styled(FormLabel)`
  && {
    font-weight: bold;
    font-size: ${props => props.theme.fontSizes.formTitle};
    color: ${props => props.theme.colors.black};
  }
`;

export const IconComponent = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

export default StyledComponent;
