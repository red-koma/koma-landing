import React from 'react';

import InputText from '../../../components/inputs/InputText';
import Link from '../../../components/buttons/Link';
import Button from '../../../components/buttons/Button';
import ButtonIcon from '../../../components/buttons/ButtonIcon';
import CloseIcon from '@material-ui/icons/Close';
import { useLanguageContext } from '../../../contexts/LanguageContext';
import {
  Container,
  Header,
  Body,
  Title,
  RightContainer,
  InputContainer,
  ButtonContainer,
  SignupContainer,
  Error,
} from './styles';
import useInputValue from '../../../hooks/useInputValue';
import { useAuthContext } from '../../../contexts/AuthContext';
import useErrorMessage from '../../../hooks/useErrorMessage';

export const Login = ({ onSubmit, onForgot, onSignup, onClose }) => {
  const { translate } = useLanguageContext();
  const { authLogin } = useAuthContext();
  const username = useInputValue('', 'email', translate('LOGIN.USERNAME'));
  const password = useInputValue('', 'password', translate('LOGIN.PASSWORD'));
  const errorMsg = useErrorMessage(3000);

  //
  const handleLogin = async () => {
    console.log('username', username);
    const response = await authLogin(username.value, password.value);

    if (response.data) {
      onSubmit(username.value);
    } else {
      errorMsg.setMessage(response.error);
    }
  };

  //
  return (
    <Container>
      <Header>
        <RightContainer>
          <ButtonIcon background={'transparent'} color="black" onClick={() => onClose()}>
            <CloseIcon />
          </ButtonIcon>
        </RightContainer>
        <Title>{translate('LOGIN')}</Title>
      </Header>
      <Body>
        <InputContainer>
          <InputText sizes="large" placeholder={translate('EMAIL')} {...username} />
        </InputContainer>
        <InputContainer>
          <InputText sizes="large" placeholder={translate('PASSWORD')} {...password} />
        </InputContainer>
        <RightContainer>
          <Link onClick={() => onForgot(username.value)}>{translate('FORGOT-YOUR-PASSWORD')}</Link>
        </RightContainer>
        {}
        <ButtonContainer>
          <Button size="large" onClick={handleLogin}>
            {translate('LOGIN')}
          </Button>
          {!!errorMsg.message && <Error>{errorMsg.message}</Error>}
        </ButtonContainer>
        <SignupContainer>
          <Link onClick={() => onSignup(username.value)}>{translate('DONT-HAVE-AN-ACCOUNT')}</Link>
        </SignupContainer>
      </Body>
    </Container>
  );
};

export default Login;
