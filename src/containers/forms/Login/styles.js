import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  border-radius: ${props => props.theme.radius.common};
  background-color: ${props => props.theme.colors.formBackground};
  box-shadow: 5px 5px 15px 0px rgba(0, 0, 0, 0.5), -1px -1px 8px 0px rgba(0, 0, 0, 0.3);
`;

export const Header = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  margin: 2vh 0 1vh;
`;

export const Body = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
`;

export const InputContainer = styled.section`
  margin: 1vh 0;
`;

export const ButtonContainer = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  margin: 2.5vh 0 1vh;
`;

export const SignupContainer = styled.section`
  margin: 0vh 0 2vh;
`;

export const RightContainer = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  width: ${props => props.theme.metrics.widths.large};
  margin-left: 1vw;
  margin-right: 1vw;
`;

export const Title = styled.span`
  font-size: ${props => props.theme.fontSizes.formTitle};
  font-family: ${props => props.theme.fonts[0]};
  margin: 20px 0 0;
`;

export const Error = styled.span`
  font-size: ${props => props.theme.fontSizes.error};
  font-family: ${props => props.theme.fonts[0]};
  color: ${props => props.theme.colors.error};
  margin: 5px 0;
`;
