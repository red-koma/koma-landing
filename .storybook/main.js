module.exports = {
  stories: ['../src/**/*.stories.js'],
  addons: [
    '@storybook/preset-create-react-app',
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-knobs',
    '@storybook/addon-a11y',
    '@storybook/addon-viewport',
    'storybook-addon-playroom',
    'storybook-addon-theme-playground/dist/register',
    '@storybook/addon-contexts/register',
    '@storybook/addon-docs',
  ],
};
