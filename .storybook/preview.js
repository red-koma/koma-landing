import { addParameters } from '@storybook/react';

addParameters({
  playroom: {
    url: 'http://localhost:9000', // your Playroom URL (default)
  },
});
