module.exports = {
  components: './src/components',
  outputPath: './dist/playroom',

  // Optional:
  title: 'Playground KOMA',
  //   themes: './src/themes',
  //   snippets: './playroom/snippets.js',
  //   frameComponent: './playroom/FrameComponent.js',
  widths: [320, 375, 768, 1024],
  port: 9000,
  openBrowser: true,
  paramType: 'search', // default is 'hash'
  exampleCode: `
      <Button onClick={console.log('Test')}>
        Hello World!
      </Button>
    `,
  baseUrl: '/playroom/',
  webpackConfig: () => ({
    // Custom webpack config goes here...
  }),
  iframeSandbox: 'allow-scripts',
};
